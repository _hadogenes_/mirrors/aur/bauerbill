#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright (C) 2015-2021 Xyne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# (version 2) as published by the Free Software Foundation.
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import collections
import colorsysplus
import fnmatch
import logging
import os
import shlex
import sys
import textwrap
import time
import urllib.error

import pyalpm

import AUR.AurPkg
import AUR.PkgList
import AUR.RPC
import AUR.SRCINFO
import MemoizeDB
import pm2ml
import Powerpill
import XCGF
import XCPF


# --------------------------------- Globals ---------------------------------- #

BB_ARGS = {
    'aur': 'Enable AUR support.',
    'aur-only': 'Enable AUR support and limit some operations such as system upgrades to AUR packages.',
    'bb-config': 'The bauerbill JSON configuration file.',
    'bb-quiet': 'Suppress warning messages for AUR operations.',
    'build-all': 'Build targets and dependencies.',
    'build': 'Build target packages that can be built.',
    'build-dir': 'The directory in which to save generated scripts.',
    'build-vcs': 'Rebuild all rebuildable VCS packages (e.g. foo-git, bar-hg)',
    'nobqd': '(no build queue deps) Do not install calculated sync deps for build queue directly. Use this option to allow makepkg to handle all sync deps. The advantage is that the build scripts can be generated without root. The disadvantage is that some deps may be installed and removed multiple times if they are required by multiple build targets.',
}
#   'build-world' : 'Rebuild all rebuildable installed packages.',

BB_PARAM_ARGS = {
    'bb-config': '/etc/bauerbill/bauerbill.json',
    'build-dir': 'build',
}

RUN_HOOK_SCRIPTS = 'run_hook_scripts'
WAIT_PIDS = 'wait_pids'
INSTALL_PACKAGE = 'pkg_install'

SCRIPT_HEADER = '#!/bin/bash\nset -e\n'

RUN_HOOK_SCRIPTS_FUNCTION = '''function {}()
{{
  scriptdir="$1"
  shift 1
  if [[ -d $scriptdir ]]
  then
    shopt_nullglob="$(shopt -p nullglob)"
    shopt -s nullglob
    for script_ in "$scriptdir"/*
    do
      if [[ -x $script_ ]]
      then
        "$script_" "$@"
      fi
    done
    $shopt_nullglob
  fi
}}
'''.format(RUN_HOOK_SCRIPTS)

WAIT_PIDS_FUNCTION = '''function {}()
{{
  for pid in "$@"
  do
    while [[ -e /proc/$pid ]]
    do
      #echo "waiting for $pid"
      sleep 0.5
    done
  done
}}
'''.format(WAIT_PIDS)

INSTALL_PACKAGE_FUNCTION = '''function {}()
{{
  target_pkgname_="$1"
  pacman_config_="$2"
  shift 2
  makepkg --packagelist | while read pkg_
  do
    pkgname_="${{pkg_%-*-*-*}}"
    if [[ $pkgname_ == $target_pkgname_ ]]
    then
      sudo pacman --config "$pacman_config_" -U ./"$pkg_"* "$@"
      pacman --config "$pacman_config_" -T "$target_pkgname_" || exit 1
    fi
    break
  done
}}
'''.format(INSTALL_PACKAGE)

PACMAN_CONFIG_PLACEHOLDER = 'PacmanConfig'


# ----------------------------- Argument Parsing ----------------------------- #

def parse_args(args=None):
    if args is None:
        args = sys.argv[1:]

    pp_param_opts = Powerpill.PACMAN_PARAM_OPTS | Powerpill.POWERPILL_PARAM_OPTS

    pargs = {
        'powerpill_args': list(),
    }
    for arg in BB_ARGS:
        try:
            if BB_PARAM_ARGS[arg] is not None:
                pargs[arg] = BB_PARAM_ARGS[arg]
        except KeyError:
            pargs[arg] = False

    argq = collections.deque(XCGF.expand_short_args(args))
    while argq:
        arg = argq.popleft()
        if arg[:2] == '--' and arg[2:] in BB_ARGS:
            if arg[2:] in BB_PARAM_ARGS:
                try:
                    pargs[arg[2:]] = argq.popleft()
                except IndexError:
                    raise ArgumentError('no argument given for "{}"\n'.format(arg))
            else:
                pargs[arg[2:]] = True
        else:
            pargs['powerpill_args'].append(arg)
            if arg in pp_param_opts:
                pargs['powerpill_args'].append(argq.popleft())

    if pargs['aur-only']:
        pargs['aur'] = True
    if pargs['build-all']:
        pargs['build'] = True

    return pargs


def bb_arguments_help_message(indent=0):
    msg = ''
    for k, v in sorted(BB_ARGS.items(), key=lambda x: x[0]):
        if k in BB_PARAM_ARGS:
            k = '{} <{}>'.format(k, k.upper())
        msg += '''{indent}--{k}
{indent}    {v}

'''.format(indent=(indent * '  '), k=k, v=v)
    return msg


def display_help():
    '''
    Print the help message.
    '''
    name = os.path.basename(sys.argv[0])
    title = name.title()

    print('''USAGE
  {name} [{name} options] [pacman args]

OPTIONS
  {title} should accept the same arguments as Powerpill, e.g.

      {name} -Syu

  See "pacman --help" for further help.

  The following additional arguments are supported:

{arguments}
'''.format(
        name=name,
        title=title,
        arguments=bb_arguments_help_message(indent=2)
    )
    )


# -------------------------------- Exceptions -------------------------------- #

class BauerbillError(XCPF.XcpfError):
    '''Parent class of all custom exceptions raised by this module.'''

    def __str__(self):
        return '{}: {}'.format(self.__class__.__name__, self.msg)


class ConfigError(BauerbillError):
    '''Exceptions raised by the Config class.'''
    pass


class ArgumentError(BauerbillError):
    '''Exceptions raised by the Config class.'''
    pass


# --------------------------------- Scripts ---------------------------------- #

def sh_info_comments(pkgs):
    '''
    Comments containing information about the package.
    '''
    if isinstance(pkgs, list):
        pkg = pkgs[0]
    else:
        pkg = pkgs
        pkgs = None

    info = '''# Maintainer(s): {maintainers}
# Last modified: {mtime}
# Repository: {repo}'''.format(
        maintainers=' '.join(pkg.maintainers()),
        mtime=time.strftime(XCGF.DISPLAY_TIME_FORMAT, time.localtime(pkg.last_modified())),
        repo=pkg.repo()
    )

    if pkgs:
        info += '''
# Packages: {pkgnames}'''.format(
            pkgnames=' '.join(p.pkgname() for p in pkgs)
        )
    else:
        info += '''
# Package Base: {pkgbase}'''.format(
            pkgbase=pkg.pkgbase(),
        )

    return info


def iterate_hook_targets(pbs, as_dict=False):
    '''
    Convenience function for iterating over runscript targets.
    '''
    # This originally accepted Aur packages which are just dictionaries so
    # checking for isinstance(pbs, dict) did not work. I'm leaving this as-is for
    # now.
    if as_dict:
        for pkgbase, pkgs in sorted(pbs.items(), key=lambda x: x[0]):
            pkg = pkgs[0]
            yield pkg
    else:
        if not isinstance(pbs, list):
            pbs = [pbs]

        for pkg in sorted(pbs):
            yield pkg


def sh_hooks_block(bb_config, pacman_config, typ, pbs, as_dict=False):
    '''
    Get runscripts block.
    '''
    block = ''

    cpath = ('hooks', 'commands', typ)
    scriptdir = XCGF.get_nested_key_or_none(bb_config, ('hooks', 'directory'))
    hook_cmds = XCGF.get_nested_key_or_none(bb_config, cpath)
    if not (scriptdir or hook_cmds):
        return block

    for pkg in iterate_hook_targets(pbs, as_dict=as_dict):
        kwargs = dict(XCPF.ArchPkg.BuildablePkgMapping(pkg))
        kwargs[PACMAN_CONFIG_PLACEHOLDER] = pacman_config

        if scriptdir:
            dpath = os.path.join(scriptdir, pkg.pkgbase(), typ)
            cmd = [RUN_HOOK_SCRIPTS, dpath]

            args = get_matching_cmd(bb_config, ('hooks', 'arguments'), pkg, required=False)
            if args:
                args = XCGF.sh_quote_words(args, kwargs=kwargs)
                cmd.extend(args)
            block += XCGF.sh_quote_words(cmd) + '\n'

        if hook_cmds:
            cmds = get_matching_cmd(bb_config, cpath, pkg, required=False)
            if cmds:
                for cmd in cmds:
                    if cmd:
                        cmd = XCGF.sh_quote_words(cmd, kwargs=kwargs)
                        if cmd:
                            block += cmd + '\n'
    return block


def get_matching_cmd(bb_config, cpath, pkg, required=True):
    '''
    Match package against custom and VCS pattern to determine which value to
    return.
    '''
    cpath = list(cpath)
    value = None
    pkgbase = pkg.pkgbase()
    qualified_pkgbase = pkg.qualified_pkgbase()

    custom_cmds = XCGF.get_nested_key_or_none(bb_config, cpath + ['custom'])
    if custom_cmds:
        for pattern, c in custom_cmds:
            if fnmatch.fnmatch(qualified_pkgbase, pattern):
                value = c

    if not value:
        vcs_cmd = XCGF.get_nested_key_or_none(bb_config, cpath + ['VCS'])
        if vcs_cmd:
            vcs_patterns = XCGF.get_nested_key_or_none(bb_config, ['VCS patterns'])
            if vcs_patterns:
                for pattern in vcs_patterns:
                    if fnmatch.fnmatch(pkgbase, pattern):
                        value = vcs_cmd

    if not value:
        value = XCGF.get_nested_key_or_none(bb_config, cpath + ['default'])

    if not value:
        if required:
            raise ConfigError('missing default: {}'.format(cpath))

    value = value.copy()
    common_args = XCGF.get_nested_key_or_none(bb_config, cpath + ['common arguments'])
    if common_args:
        value.extend(common_args)

    return value


def get_bin(bb_config, cmd):
    try:
        return bb_config['bin'][cmd].copy()
    except KeyError:
        return [cmd]


def get_query_trust_cmd(bb_config, pkg, subdir=False):
    '''
    Get the query trust command to prompt the user if necessary.
    '''
    qt_cmd = get_bin(bb_config, 'bb-query_trust')
    path = '.'
    if subdir:
        path = os.path.join(path, pkg.pkgbase())
    if isinstance(pkg, AUR.AurPkg.AurBuildablePkg):
        AUR.RPC.add_last_packager(pkg.pkg)
        last_packager = pkg.last_packager()
        if last_packager:
            maintainers = [
                last_packager,
                *(mntr for mntr in pkg.maintainers() if mntr != last_packager)
            ]
        else:
            maintainers = pkg.maintainers()
    else:
        maintainers = pkg.maintainers()
    qt_cmd.extend((
        '-l', path,
        pkg.qualified_pkgbase(),
        pkg.last_modified(),
        *maintainers
    ))
    return '{} || exit 1'.format(XCGF.sh_quote_words(qt_cmd))


def save_scripts(dpath, scripts, print_msg=False):
    '''
    Save the scripts to the output directory and chown them as necessary.
    '''
    paths = [dpath]
    try:
        os.mkdir(dpath)
    except FileExistsError:
        pass
    for name, script in scripts.items():
        path = os.path.join(dpath, '{}.sh'.format(name))
        paths.append(path)
        with open(path, 'w') as f:
            f.write(script)
        msg = 'created {}'.format(path)
        logging.info('created {}'.format(path))
        if print_msg:
            print(msg)

    name, uid, gid = XCGF.get_sudo_user_info()

    for p in paths:
        os.chmod(p, 0o755)
        if uid is not None:
            os.chown(p, uid, gid)


# ----------------------------- Bauerbill Class ------------------------------ #

class Bauerbill(Powerpill.Powerpill):
    def __init__(self, pargs):

        self.pargs = pargs
        pp_pargs = Powerpill.parse_args(pargs['powerpill_args'])

        pm2ml_args = list()
        for a in ('aur', 'aur-only'):
            if pargs[a]:
                pm2ml_args.append('--{}'.format(a))
        pm2ml_pargs = pm2ml.parse_args(pm2ml_args)

        try:
            self.config = XCGF.load_json(pargs['bb-config'])
        except (ValueError, FileNotFoundError) as e:
            raise ConfigError(
                'failed to load configuration file [{}]'.format(pargs['bb-config']),
                error=e
            )

        data_ttl = pargs.get('data ttl', AUR.common.DEFAULT_TTL)
        self.powerpill = Powerpill.Powerpill(
            pp_pargs,
            pm2ml_pargs=pm2ml_pargs,
            ttl=data_ttl
        )

        if pargs['aur']:
            self.aur_pkglist = AUR.PkgList.PkgList(ttl=data_ttl, auto_refresh=True)
        else:
            self.aur_pkglist = None

    def refresh(self):
        '''
        Refresh databases and other metadata.
        '''
        if self.pargs['aur']:
            if self.powerpill.pargs['refresh'] > 1:
                # Wipe pm2ml's cached AUR data.
                self.powerpill.pm2ml.refresh_databases(opi=False)
                self.aur_pkglist.refresh(force=True)
            else:
                self.powerpill.pm2ml.aur.mdb.db_clean()
                self.aur_pkglist.refresh()

        if not self.pargs['aur-only']:
            # pm2ml's cached AUR data was wiped above: avoid a redundant wipe
            self.powerpill.refresh_databases(pm2ml_passthrough_args={'aur': False})

    def initialize_alpm(self):
        '''
        Reinitialize the ALPM database.
        '''
        self.powerpill.initialize_alpm()

    def select_installed_vcs_pkgs(self):
        '''
        Iterate over all package matching a VCS pattern defined in the Bauerbill
        configuration file.
        '''
        vcs_patterns = XCGF.get_nested_key_or_none(self.config, ('VCS patterns',))
        if vcs_patterns:
            handle = self.powerpill.pm2ml.handle
            for pkg in handle.get_localdb().pkgcache:
                for p in vcs_patterns:
                    if fnmatch.fnmatch(pkg.name, p):
                        yield pkg
                        break

    def maybe_build_vcs(self):
        if self.pargs['build-vcs']:
            self.force_build_pkgs = XCPF.ArchPkg.PyalpmPkgSet(
                p for p in self.select_installed_vcs_pkgs()
            )
            self.powerpill.pargs['args'] += list(p.name for p in self.force_build_pkgs)
        else:
            self.force_build_pkgs = XCPF.ArchPkg.PyalpmPkgSet()

    def get_sync_and_build_targets(self):
        '''
        Determine the sync and build targets for the current operation. This
        modifies powerpill.pargs to avoid redundant operations later.
        '''
        build_pkgs = XCPF.ArchPkg.BuildablePkgSet()
        build_deps = XCPF.ArchPkg.BuildablePkgSet()
        sync_pkgs = XCPF.ArchPkg.PyalpmPkgSet()
        sync_deps = XCPF.ArchPkg.PyalpmPkgSet()

        if self.powerpill.pargs['sysupgrade'] > 0 or self.powerpill.pargs['args']:
            build_patterns = XCGF.get_nested_key_or_none(self.config, ('build patterns',))
            if self.pargs['build'] or self.pargs['aur'] or build_patterns:

                pm2ml_args = self.powerpill.get_pm2ml_pkg_download_args()
                pm2ml_pargs = pm2ml.parse_args(pm2ml_args)

                sync_pkgs, sync_deps, \
                    aur_pkgs, aur_deps, \
                    not_found, unknown_deps, orphans = self.powerpill.pm2ml.resolve_targets_from_arguments(
                        pargs=pm2ml_pargs,
                    )
                self.powerpill.pargs['args'] = list(x.name for x in sync_pkgs)

                orphan_names = set(p.name for p in orphans)
                for label, xs, is_error in (
                    ('Not found', not_found, False),
                    ('Unresolved dependencies', unknown_deps, True),
                    ('Installed orphans', orphan_names, False)
                ):
                    if xs:
                        msg = '{}: {}'.format(label, ' '.join(sorted(xs)))
                        if is_error:
                            logging.error(msg)
                        else:
                            logging.warning(msg)

                if self.pargs['aur']:
                    build_pkgs = XCPF.ArchPkg.BuildablePkgSet(self.powerpill.pm2ml.buildable_pkgs(aur_pkgs | aur_deps))
                    build_deps = XCPF.ArchPkg.BuildablePkgSet(self.powerpill.pm2ml.buildable_pkgs(aur_deps))
                    self.powerpill.pargs['args'] = list(
                        a for a in self.powerpill.pargs['args'] if a not in build_pkgs
                    )

                # User-selected autobuild packages. Patterns with "/" are matched
                # against the qualified name (<repo>/</pkgname>). Patterns without are
                # matched against the unqualified name (<pkgname>).
                if build_patterns:
                    self.force_build_pkgs |= XCPF.ArchPkg.PyalpmPkgSet(
                        p for p in (sync_pkgs | sync_deps)
                        if any(
                            (
                                fnmatch.fnmatch('{}/{}'.format(p.db.name, p.name), bp)
                                if '/' in bp
                                else fnmatch.fnmatch(p.name, bp)
                            )
                            for bp in build_patterns
                        )
                    )

        # This can be used to avoid querying the GIT interface if there are only AUR
        # packages for the download.
        self.pargs['pbget repo packages'] = False
        # TODO
        # Carefully consider if naming conflicts can arise when unioning packages
        # from different sources in the BuildablePkgSets.
        if self.pargs['build']:
            if sync_pkgs:
                # This can be used to avoid querying the GIT interface if there are only
                # AUR packages for the download.
                self.pargs['pbget repo packages'] = True
                build_pkgs |= XCPF.ArchPkg.BuildablePkgSet(
                    self.powerpill.pm2ml.buildable_pkgs(sync_pkgs)
                )

            if self.pargs['build-all']:
                if sync_deps:
                    self.pargs['pbget repo packages'] = True
                build_pkgs |= XCPF.ArchPkg.BuildablePkgSet(
                    self.powerpill.pm2ml.buildable_pkgs(sync_deps)
                )
                build_deps |= XCPF.ArchPkg.BuildablePkgSet(
                    self.powerpill.pm2ml.buildable_pkgs(sync_deps)
                )
                self.powerpill.pargs['args'].clear()

            else:
                if self.pargs['nobqd']:
                    self.powerpill.pargs['args'].clear()
                else:
                    self.powerpill.pargs['options'].append('--asdeps')
                    self.powerpill.pargs['args'] = list(x.name for x in sync_deps)

        # Build packages selected by user.
        elif self.force_build_pkgs:
            if sync_pkgs:
                force_build_sync_pkgs = sync_pkgs & self.force_build_pkgs
                if force_build_sync_pkgs:
                    # pkginfo_retriever = memoized_archlinux_org_pkg_info_function(powerpill, data_ttl)
                    forced_pkgs = XCPF.ArchPkg.BuildablePkgSet(
                        self.powerpill.pm2ml.buildable_pkgs(force_build_sync_pkgs)
                    )
                    self.pargs['build'] = self.pargs['pbget repo packages'] = bool(forced_pkgs)
                    build_pkgs |= forced_pkgs
                    sync_pkgs -= self.force_build_pkgs
            self.powerpill.pargs['args'] = list(
                p for p in self.powerpill.pargs['args'] if p not in self.force_build_pkgs
            )

        return sync_pkgs, sync_deps, build_pkgs, build_deps

    # -------------------------------- Scripts --------------------------------- #

    def generate_build_scripts(self, build_pkgs, build_deps):
        if build_pkgs:
            unbuildable_pkgs = XCPF.ArchPkg.BuildablePkgSet(
                p for p in build_pkgs if not p.buildable()
            )
            build_pkgs -= unbuildable_pkgs
            # The build_deps are a subset of build_pkgs.
            build_deps -= unbuildable_pkgs

            # Anything unbuildable must be available in a binary repo and can therefore
            # be installed by makepkg so display warnings instead of errors.
            if unbuildable_pkgs:
                logging.warning('unbuildable: {}'.format(
                    ' '.join(sorted(p.pkgname() for p in unbuildable_pkgs))
                ))

            scripts = collections.OrderedDict()
            scripts['download'] = self.download_script(build_pkgs)
            scripts['build'] = self.build_script(build_pkgs, build_deps)

    #     if not pargs['nobqd']:
            remaining_deps = set(d.pkgname() for d in build_deps)
            if remaining_deps:
                # Don't remove anything that's already installed.
                handle = self.powerpill.pm2ml.handle
                local_db = handle.get_localdb()
                remaining_deps = set(d for d in remaining_deps if not local_db.get_pkg(d))
                if remaining_deps:
                    scripts['clean'] = self.clean_script(remaining_deps)

            save_scripts(self.pargs['build-dir'], scripts, print_msg=True)

    def download_script(self, build_pkgs):
        '''
        Create a script to download the PKGBUILDs and source files.
        '''
        pbs = XCPF.ArchPkg.collect_pkgbases(build_pkgs)
        pkgbases = sorted(pbs)
        pbget_cmd = get_bin(self.config, 'pbget')

        if self.pargs['aur-only'] or not self.pargs['pbget repo packages']:
            pbget_cmd.append('--aur-only')
        elif self.pargs['aur']:
            pbget_cmd.append('--aur')
        pbget_cmd.extend(pkgbases)

        script = '{}\n{}\n{}\n'.format(
            SCRIPT_HEADER,
            RUN_HOOK_SCRIPTS_FUNCTION,
            WAIT_PIDS_FUNCTION
        )

        preget_block = sh_hooks_block(
            self.config, self.powerpill.pargs['pacman_config'], 'preget', pbs, as_dict=True
        )
        if preget_block:
            script += '''{preget_header}
{preget_block}
'''.format(
                preget_header=XCGF.sh_comment_header('Run preget scripts, if any.'),
                preget_block=preget_block
            )

        script += '''{pbget_header}
{pbget_cmd}

pids=()
'''.format(
            pbget_header=XCGF.sh_comment_header('Get PKGBUILDS and related files.'),
            pbget_cmd=XCGF.sh_quote_words(pbget_cmd),
        )

        entries = list(
            get_query_trust_cmd(
                self.config,
                pbs[pkgbase][0],
                subdir=True
            ) for pkgbase in pkgbases if not pbs[pkgbase][0].trusted()
        )
        if entries:
            script += '''
{}
{}
'''.format(
                XCGF.sh_comment_header('Query trust before starting downloads.'),
                '\n'.join(entries)
            )

        for pkgbase in pkgbases:
            pkgs = pbs[pkgbase]
            pkg = pkgs[0]
            kwargs = dict(XCPF.ArchPkg.BuildablePkgMapping(pkg))
            kwargs[PACMAN_CONFIG_PLACEHOLDER] = self.powerpill.pargs['pacman_config']

            kwargs = {
                'header': XCGF.sh_comment_header('Download sources for package base {}.'.format(pkgbase)),
                'info': sh_info_comments(pkgs),
                'quoted_pkgbase': shlex.quote(pkgbase),
                'predownload_block': sh_hooks_block(
                    self.config, self.powerpill.pargs['pacman_config'], 'predownload', pbs[pkgbase]
                ),
                'download_cmd': XCGF.sh_quote_words(
                    get_matching_cmd(self.config, ('makepkg commands', 'download'), pkg),
                    kwargs=kwargs
                )
            }

            script += '''
{header}
{info}
pushd {quoted_pkgbase}
{predownload_block}{download_cmd} &
pids+=($!)
echo {quoted_pkgbase} ": $!"
popd
'''.format(**kwargs)

        script += '''
{header}
{wait_pids} "${{pids[@]}}"
'''.format(
            header=XCGF.sh_comment_header('Wait for downloads and verifications to finish.'),
            wait_pids=WAIT_PIDS
        )
        return script

    def build_script(self, build_pkgs, build_deps):
        '''
        Create a script to build and install the target packages.
        '''
        build_graph = XCPF.ArchPkg.determine_dependency_graph(build_pkgs)
        script = '{}\n{}'.format(
            SCRIPT_HEADER,
            RUN_HOOK_SCRIPTS_FUNCTION
        )
        for pkg in XCPF.ArchPkg.determine_build_order(build_graph):
            if pkg in build_deps:
                qualifier = ' --asdeps'
            else:
                qualifier = ''
            if pkg.trusted():
                query_trust_cmd = ''
            else:
                query_trust_cmd = get_query_trust_cmd(self.config, pkg) + '\n'
            kwargs = dict(XCPF.ArchPkg.BuildablePkgMapping(pkg))
            kwargs[PACMAN_CONFIG_PLACEHOLDER] = self.powerpill.pargs['pacman_config']

            script += '''
{header}
{info}
pushd {pkgbase}
{query_trust_cmd}{prebuild_block}{build_cmd}{qualifier}
pacman --config {pconfig_path} -T {safename} || exit 1
popd
'''.format(
                header=XCGF.sh_comment_header(
                    'Build package {}'.format(pkg.pkgname())
                ),
                safename=shlex.quote(pkg.pkgname()),
                pkgbase=shlex.quote(pkg.pkgbase()),
                info=sh_info_comments(pkg),
                query_trust_cmd=query_trust_cmd,
                prebuild_block=sh_hooks_block(
                    self.config, self.powerpill.pargs['pacman_config'], 'prebuild', pkg
                ),
                build_cmd=XCGF.sh_quote_words(
                    get_matching_cmd(self.config, ('makepkg commands', 'build'), pkg),
                    kwargs=kwargs
                ),
                pconfig_path=shlex.quote(self.powerpill.pargs['pacman_config']),
                qualifier=qualifier
            )
        return script

    def clean_script(self, remaining_deps):
        '''
        Create a script to remove leftover build dependencies.
        '''
        pacman_config = self.powerpill.pargs['pacman_config']
        sudo_cmd = get_bin(self.config, 'sudo')
        pacman_cmd = get_bin(self.config, 'pacman')
        rm_cmd = sudo_cmd + pacman_cmd + ['-Runs', '--config', pacman_config]
        lst_cmd = pacman_cmd + ['-Qq', '--config', pacman_config]
        return '''{}
target_pkgs_=(
  {}
)
installed_pkgs_=($({} "${{target_pkgs_[@]}}"))
{} "${{installed_pkgs_[@]}}"
'''.format(
            SCRIPT_HEADER,
            '\n  '.join(shlex.quote(rd) for rd in sorted(remaining_deps)),
            XCGF.sh_quote_words(lst_cmd),
            XCGF.sh_quote_words(rm_cmd)
        )

    # -------------------------------- Display --------------------------------- #

    def display_sync_info(self):
        '''
        Emulate "pacman -Si".
        '''
        pm2ml_args = ['--nodeps'] + self.powerpill.pargs['args']
        pm2ml_pargs = pm2ml.parse_args(pm2ml_args)

        sync_pkgs, sync_deps, \
            aur_pkgs, aur_deps, \
            not_found, unknown_deps, orphans = self.powerpill.pm2ml.resolve_targets_from_arguments(
                pm2ml_pargs
            )

        for p in sync_pkgs:
            print(XCPF.format_pkginfo(p))

        if aur_pkgs:
            for i in AUR.RPC.format_pkginfo(aur_pkgs):
                print(i)

        if not_found:
            msg = 'not found: {}'.format(' '.join(not_found))
            logging.error(msg)
            return 1
        else:
            return 0

    # community/i3-wm 4.11-1 (i3) [installed]
    #     An improved dynamic tiling window manager

    def display_sync_search(self, args=None):
        '''
        Emulate "pacman -Ss"
        '''
        if self.pargs['aur-only']:
            e = 0
        else:
            e = self.powerpill.run_pacman()
        if self.powerpill.pm2ml.aur is None:
            return e

        found_all = None
        if args is None:
            args = self.powerpill.pargs['args']
        for term in args:
            found = AUR.AurPkg.AurPkgSet(self.powerpill.pm2ml.aur.search(term))
            if found_all is None:
                found_all = found
            else:
                found_all &= found

        if found_all:
            if not self.powerpill.pargs['quiet']:
                handle = self.powerpill.pm2ml.handle
                local_db = handle.get_localdb()
                color = self.powerpill.use_color()
            else:
                local_db = None
                color = False

            # *_ce: color escape
            if color:
                repo_ce = colorsysplus.ansi_sgr(fg=13)
                installed_ce = colorsysplus.ansi_sgr(fg=14)
                reset_ce = colorsysplus.ansi_sgr(reset=True)
            else:
                repo_ce = ''
                installed_ce = ''
                reset_ce = ''

            for pkg in sorted(found_all, key=lambda p: p['Name']):
                if local_db is None:
                    print(pkg['Name'])

                else:
                    if local_db.get_pkg(pkg['Name']):
                        tag = ' {}[installed]'.format(installed_ce)
                    else:
                        tag = ''
                    print('''{repo_ce}AUR{reset_ce}/{name}{tag}{reset_ce}\n    {desc}'''.format(
                        name=pkg['Name'],
                        tag=tag,
                        desc='\n    '.join(textwrap.wrap(pkg['Description'], width=80 - 4)),
                        repo_ce=repo_ce,
                        reset_ce=reset_ce
                    ))
        return e

    def display_aur_list(self):
        # <repo> <pkgname> <pkgver> [installed]
        # Version is missing and not worth retrieving via RPC.
        # *_ce: color escape
        if not self.powerpill.pargs['quiet']:
            handle = self.powerpill.pm2ml.handle
            local_db = handle.get_localdb()
        else:
            local_db = None
        if self.powerpill.use_color():
            repo_ce = colorsysplus.ansi_sgr(fg=13)
            installed_ce = colorsysplus.ansi_sgr(fg=14)
            reset_ce = colorsysplus.ansi_sgr(reset=True)
        else:
            repo_ce = ''
            installed_ce = ''
            reset_ce = ''
        if self.aur_pkglist:
            for p in sorted(self.aur_pkglist):
                if local_db is None:
                    print(p)
                else:
                    if local_db.get_pkg(p):
                        tag = ' {}[installed]'.format(installed_ce)
                    else:
                        tag = ''
                    print('{repo_ce}AUR{reset_ce} {pkg}{tag}{repo_ce}'.format(
                        pkg=p,
                        tag=tag,
                        repo_ce=repo_ce,
                        reset_ce=reset_ce
                    ))

    def display_sync_list(self):
        '''
        Emulate "pacman -Sl".
        '''
        if not self.powerpill.pargs['args']:
            if not self.pargs['aur-only']:
                e = self.powerpill.run_pacman()
            else:
                e = None
            if not e:
                self.display_aur_list()
            return e
        # TODO
        # Improve this.
        else:
            e = None
            if 'AUR' in self.powerpill.pargs['args']:
                for a in list(self.powerpill.pargs['args']):
                    if e:
                        break
                    elif a == 'AUR':
                        self.display_aur_list()
                    else:
                        self.powerpill.pargs['args'] = [a]
                        e = self.powerpill.run_pacman()
            else:
                e = self.powerpill.run_pacman()
            return e

    def display_query_upgrades(self):
        '''
        Emulate "pacman -Qu".
        '''
        if self.pargs['aur-only']:
            e = 0
        else:
            e = self.powerpill.run_pacman()
        if self.powerpill.pm2ml.aur is None:
            return e

        pm2ml_args = self.powerpill.get_pm2ml_pkg_download_args(ignore=False)
        pm2ml_pargs = pm2ml.parse_args(pm2ml_args)
        handle = self.powerpill.pm2ml.handle

        sync_pkgs, sync_deps, \
            aur_pkgs, aur_deps, \
            not_found, unknown_deps, orphans = self.powerpill.pm2ml.resolve_targets_from_arguments(
                pm2ml_pargs, ignore_ignored=True,
            )

        if aur_pkgs:
            # *_ce: color escape
            if self.powerpill.use_color():
                version_ce = colorsysplus.ansi_sgr(fg=10)
                reset_ce = colorsysplus.ansi_sgr(reset=True)
            else:
                version_ce = ''
                reset_ce = ''

            show = set(self.powerpill.pargs['args'])
            local_db = handle.get_localdb()
            for pkgname, pkg in sorted(aur_pkgs.pkgs.items()):
                if show and pkgname not in show:
                    continue

                if self.powerpill.pargs['quiet']:
                    print(pkgname)

                else:
                    local_pkg = local_db.get_pkg(pkgname)
                    if local_pkg:
                        if pkgname in self.powerpill.pacman_conf.options['IgnorePkg']:
                            ignored = ' [ignored]'
                        else:
                            ignored = ''
                        print('{pkgname} {vce}{locver}{rce} -> {vce}{aurver}{rce}{ignored}'.format(
                            pkgname=pkgname,
                            vce=version_ce,
                            rce=reset_ce,
                            aurver=pkg['Version'],
                            locver=local_pkg.version,
                            ignored=ignored
                        ))
        return e


# ----------------------------------- Main ----------------------------------- #

# This should follow the main function of Powerpill.
def main(args=None):
    pargs = parse_args(args)

    pp_pargs = Powerpill.parse_args(pargs['powerpill_args'])
    if pp_pargs['help']:
        display_help()
        return 0

    Powerpill.configure_logging(pp_pargs, pargs['bb-quiet'])

    bb = Bauerbill(pargs)

    exit_code = 0

    # Clean up before doing anything else.
    if bb.powerpill.pargs['powerpill_clean']:
        bb.powerpill.clean()
        if bb.powerpill.no_operation():
            return 0

    if not bb.powerpill.pargs['sync']:
        # -Qu --aur
        if pargs['aur'] and bb.powerpill.query_upgrades():
            return bb.display_query_upgrades()
        else:
            return bb.powerpill.run_pacman()

    if bb.powerpill.pargs['refresh'] > 0:
        bb.refresh()

    # Check for upgradable VCS packages here to ensure that they are considered
    # when determining which operations will be performed in the block below.
    bb.maybe_build_vcs()

    if bb.powerpill.no_download():
        if bb.powerpill.other_operation():
            if bb.powerpill.info_operation():
                return bb.display_sync_info()
            elif bb.powerpill.search_operation():
                return bb.display_sync_search()
            elif bb.powerpill.list_operation():
                return bb.display_sync_list()
            else:
                return bb.powerpill.run_pacman()
        return 0

    sync_pkgs, sync_deps, build_pkgs, build_deps = bb.get_sync_and_build_targets()

    if pargs['build'] or pargs['aur-only']:
        bb.powerpill.pargs['sysupgrade'] = 0

    if not bb.powerpill.no_download():
        bb.powerpill.download_packages()

    if bb.powerpill.proceed_to_installation():
        exit_code = bb.powerpill.run_pacman()
        bb.initialize_alpm()

    bb.generate_build_scripts(build_pkgs, build_deps)

    return exit_code


def run_main(args=None):
    try:
        return main(args)
    except (KeyboardInterrupt, BrokenPipeError):
        pass
    except (
        PermissionError,
        Powerpill.PowerpillError,
        pyalpm.error,
        XCGF.LockError,
        XCPF.XcpfError,
        MemoizeDB.MDBError,
        urllib.error.URLError
    ) as e:
        logging.error(str(e))
        return 1


if __name__ == '__main__':
    try:
        sys.exit(run_main())
    except KeyboardInterrupt:
        pass
