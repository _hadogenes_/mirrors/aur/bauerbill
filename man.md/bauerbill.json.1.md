# Name

bauerbill.json - Bauerbill configuration file


# Description
The Bauerbill configuration file is a plain JSON file. By default it is located at `/etc/bauerbill/bauerbill.conf`. The main object is a dictionary that holds multiple dictionaries. The latter are considered sections of the configuration file and contain options related to different parts of Bauerbill.


# Sections
Note that all fields, including section names, are in lower case in the file. Upper case may appear in the man page during automatic conversion of the markdown file. For example, the first section is `makepkg commands`, *not* `MAKEPKG COMMANDS`.

## bin

A mapping of expected executables to their commands, e.g. `"pacman": ["/usr/bin/pacman"]`. These can be used to wrap other commands or enforce certain options.

## build patterns

A list of glob patterns. Patterns with "/" are matched against qualified package names (\<repo\>/\<pkgname\>). Patterns without are matched against unqualified package names (\<pkgname\>). Any package that matches is added to the build queue.

## data ttl

The default time-to-live for cached data.

## VCS patterns

This is just a list of glob patterns to match VCS packages, e.g. `["*-git", "*-svn"]`.

## makepkg commands

This section contains 2 subsections: `build` and `download`. These determine the makepkg commands to use for building and download packages, respectively. These commands can be configured per package using glob patterns along with a default command. Both subsections follow the same format with the following keys:

custom
:   This key may be `null` or omitted. If included, it must be a list of 2-tuples. The first item is a glob pattern that will be tested against "\<repo\>/\<pkgbase\>" (e.g. "*/reflector" will match "community/reflector" and "AUR/*" will match all AUR packages). The second item is a makepkg command with arguments as a list, e.g `['makepkg', '-irs', '--config', '/path/to/custom/makepkg.conf']`. This command **must** install the package and accept `--asdeps` and `--pkg` as possible arguments. Simple example: `"custom" : [["AUR/*", ["makepkg", "-irs", "--config", "/path/to/custom/makepkg.conf"]]]`

VCS
:   A custom command for packages that match the patterns in `VCS patterns`. This key may be `null` or omitted.

default
:   The command to run if no match occurs.

common arguments
:   Arguments that will be appended to all commands in the section.

Each command must be a list where each item is a shell "word", e.g. `["makepkg", "-irs", "--config", "/path/to/some/config"]`. Each item will be interpolated using Python string formatting so curly braces must be escaped (i.e. "{" should be "{{" and "}" should be "}}"). The possible mappings are the names in `BuildablePkgMapping` in `Bauerbill.py` (e.g. `["{Name}", "{LastModified}"]`) and "PacmanConfig".

Additional makepkg arguments will be added to the command as necessary when generating the different scripts, for example `--asdeps` or `--pkg`.

## hooks

### Hook types

preget
:   Run before the PKGBUILDs are download.

predownload
:   Run before the sources are download.

prebuild
:   Run before the package is build.


### Keys

directory
:   A path to a directory with hook scripts. This directory should contain subdirectories with package base names, which in turn contain subdirectories with the names of the hook types mentioned above. For example, setting this to `/tmp/bbhooks` will then look for scripts in `/tmp/bbhooks/linux/prebuild` before building the `linux` package.

arguments
:   This follows the same format as the makepkg commands sections above (default, VCS, custom), but with lists of arguments rather than commands. The first match will return the arguments to pass to the hook scripts. The arguments can be interpolated, e.g. `["{PackageBase}", "{LastModified}"]`. This section may be empty.

commands
;   This also follows the same format as the makepkg commands sections above but each value is a list of lists where the inner lists are commands to be run. Their arguments will also be interpolated. This section may be empty.

