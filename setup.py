#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
    name='Bauerbill',
    version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(  1640928119)),
    description='''Extension of Powerpill with AUR and ABS support.''',
    author='Xyne',
    author_email='gro xunilhcra enyx, backwards',
    url='''http://xyne.dev/projects/bauerbill''',
    py_modules=['Bauerbill'],
    scripts=['bauerbill', 'bb-query_trust']
)
