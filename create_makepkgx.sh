#!/bin/bash
set -eu
cp /usr/bin/makepkg ./makepkgx
patch ./makepkgx ./makepkg.patch
rm -f ./makepkgx.orig
echo done
